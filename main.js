// lấy json lên
var dataJson = localStorage.getItem("DSSV");
if (dataJson != null) {
  dssv = JSON.parse(dataJson).map(function (item) {
    // .map(function) để dùng được function  tính điểm trung bình
    return new SinhVien(item.ma, item.ten, item.email, item.matKhau, item.toan, item.ly, item.hoa);
  });
  renderDSSV(dssv);
}

dssv = [];
// them sinh viên
function themSinhVien() {
  // lấy thong tin từ form
  var sv = layThongTinTuForm();

  //  b1: tạo object b2 tạo new object
  //   var sv = new SinhVien(ma, ten, email, matKhau, toan, ly, hoa); cái này bỏ vô var sv = layThongTinTuForm(); bên controller

  //   push dssv
  dssv.push(sv);

  renderDSSV(dssv);
  //   lưu json xuống | load trang sẽ ko mất
  var dataJson = JSON.stringify(dssv);
  localStorage.setItem("DSSV", dataJson);
}
// xóa sinh vien
function xoaSinhVien(id) {
  //  dùng splice findIndex w3
  var index = dssv.findIndex(function (item) {
    // index là vị trí
    return item.ma == id; // cái mã phải trùng id. để nhận biết nó xóa
  });
  dssv.splice(index, 1); // index là vị trí, 1 là số lượng xóa
  renderDSSV(dssv);
}
// sửa sinh viên

function suaSinhVien(id) {
  // tìm vị trí sv trong dssv có mã trùng với id của onlick
  var index = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  ShowThongTinLenFrom(dssv[index]); // dssv[index] là dssv tại vị trí index
}

function capNhatSinhVien(id,SinhVien) {
  var index = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  if (index !== -1) {
    dssv[index] = SinhVien;
    alert("Cập nhật sinh viên thành công!");
  } else {
    alert("Không tìm thấy sinh viên trong danh sách!");
  }
    // Lưu danh sách sinh viên vào localStorage
    localStorage.setItem("DSSV", JSON.stringify(dssv));

    // Reset form
    resetForm();
}
