// đây là giao diện|| mỗi khi gọi renderDSSV(dssv) sẽ xuất hiện giao diện
function renderDSSV(dssv) {
  contentHTML = "";
  for (let i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    var content = `<tr>
  <td>$${sv.ma}</td>
  <td>${sv.ten}</td>
 <td>${sv.email}</td>
 <td>${sv.tinhDTB()}</td>
 <td>  
 <button onclick="xoaSinhVien('${sv.ma}')"  type="button"  class="btn btn-danger">xóa</button>
 <button onclick="suaSinhVien('${sv.ma}')" type="button"class="btn btn-warning">sửa</button>
</td>

 </tr>`;
    contentHTML += content;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

//  chức năng của sửa thông tin|| đem thông tin lên lại form
function ShowThongTinLenFrom(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}
//  lấy thông tin từ form và dùng lại ở chức nắng cập nhật function
function layThongTinTuForm() {
  // có return vì nó phải trả thông tin rồi cho hàm khác xử lý tiếp.
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value * 1;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;
  // chạy xong trả kết quả đem đi function khác xử lý tiếp, thì bắt buộc return || nên lúc đầu ở main ko cần return, ở đây dùng lại ở cập nhật nên return

  return new SinhVien(ma, ten, email, matKhau, toan, ly, hoa); //  cú pháp class ~ nhớ đúng thứ tự nhé ! xem thêm ở "index.js"
}
